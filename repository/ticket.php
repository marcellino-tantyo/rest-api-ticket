<?php

class Ticket
{
    // database connection and table name
    private $connection;
    private $table_name = "tickets";

    // object properties
    public $ticket_code;
    public $status;
    public $event_id;
    public $created_at;
    public $updated_at;

    // constructor database connection
    public function __construct($db)
    {
        $this->connection = $db;
    }

    function read() {
        $query = "SELECT ticket_id, ticket_code, status, event_id, created_at
                  FROM " . $this->table_name . "
                  WHERE ticket_code = ?
                  AND event_id = ?
                  ORDER BY ticket_id";

        $stmt = $this->connection->prepare($query);

        $stmt->bindParam(1, $this->ticket_code);
        $stmt->bindParam(2, $this->event_id);

        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->ticket_code = $row['ticket_code'];
        $this->status = $row['status'];
    }

    function create() {
        $query = "INSERT INTO " . $this->table_name . " SET ticket_code = :ticket_code, status = :status, event_id = :event_id, created_at = :created_at";

        $stmt = $this->connection->prepare($query);

        $this->ticket_code=htmlspecialchars(strip_tags($this->ticket_code));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->event_id=htmlspecialchars(strip_tags($this->event_id));
        $this->created_at=htmlspecialchars(strip_tags($this->created_at));

        $stmt->bindParam(":ticket_code", $this->ticket_code);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":event_id", $this->event_id);
        $stmt->bindParam(":created_at", $this->created_at);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    function update() {
        $query = "UPDATE " . $this->table_name . " SET status = :status WHERE ticket_code = :ticket_code AND event_id = :event_id";

        $stmt = $this->connection->prepare($query);

        $this->ticket_code=htmlspecialchars(strip_tags($this->ticket_code));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->event_id=htmlspecialchars(strip_tags($this->event_id));

        $stmt->bindParam(":ticket_code", $this->ticket_code);
        $stmt->bindParam(":status", $this->status);
        $stmt->bindParam(":event_id", $this->event_id);

        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
}