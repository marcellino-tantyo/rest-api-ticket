# README #

This README would normally document whatever steps are necessary to get the application up and running.

### Query ###

* The query has been placed on query.sql file to create table and insert the dummy data

### URL POSTMAN ###

* Show ticket data
* http://localhost/api-ticket/service/read.php?ticket_code=DTK02BHB91&event_id=2
* Update status ticket with body example
* http://localhost/api-ticket/service/update.php
* With body request to update data for example
  {
  "ticket_code" : "DTK02BHB95",
  "status" : "available",
  "event_id" : 1
  }