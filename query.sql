-- create table
CREATE TABLE tickets (
 ticket_id INT NOT NULL AUTO_INCREMENT,
 ticket_code VARCHAR(255) NOT NULL,
 status VARCHAR(10) NOT NULL,
 event_id INT NOT NULL,
 created_at DATETIME NOT NULL,
 updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (ticket_id)
);


-- insert data dummy
INSERT INTO api_ticket.tickets
(ticket_id, ticket_code, status, event_id, created_at, updated_at)
VALUES(1, 'DTK01AHB89', 'available', 1, '2022-03-05 18:29:46', '2022-03-05 18:29:46');
INSERT INTO api_ticket.tickets
(ticket_id, ticket_code, status, event_id, created_at, updated_at)
VALUES(2, 'DTK02BHB95', 'available', 1, '2022-03-05 19:09:27', '2022-03-06 15:47:44');
INSERT INTO api_ticket.tickets
(ticket_id, ticket_code, status, event_id, created_at, updated_at)
VALUES(3, 'DTK02BHB91', 'available', 2, '2022-03-05 14:42:58', '2022-03-05 20:42:58');
INSERT INTO api_ticket.tickets
(ticket_id, ticket_code, status, event_id, created_at, updated_at)
VALUES(4, 'DTKOXR9Z34', 'available', 1, '2022-03-06 06:25:05', '2022-03-06 12:25:05');
INSERT INTO api_ticket.tickets
(ticket_id, ticket_code, status, event_id, created_at, updated_at)
VALUES(5, 'DTKOAC6PN9', 'available', 1, '2022-03-06 06:25:05', '2022-03-06 12:25:05');
