<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../repository/ticket.php';

$database = new Database();
$db = $database->getConnection();

$ticket = new Ticket($db);

// get post data
$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if (!empty($argv[1]) && !empty($argv[2])) {
    for ($i = 0; $i < $argv[2]; $i++) {
        // set property values
        $ticket->ticket_code = "DTK" . random_string();
        $ticket->status = "available";
        $ticket->event_id = $argv[1];
        $ticket->created_at = date('Y-m-d H:i:s');

        if ($ticket->create()) {

            http_response_code(200);

            echo json_encode(array("message" => "Ticket was created."));
        } else {
            // set response code - 503 service unavailable
            http_response_code(503);
            // response message
            echo json_encode(array("message" => "Unable to create ticket"));
        }
    }
} else {
    // set response code - 400 bad request
    http_response_code(400);
    // response message
    echo json_encode(array("message" => "Unable to create ticket. Data is incomplete"));
}

// function to create random string
function random_string() {
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle($str_result), 0, 7);
}