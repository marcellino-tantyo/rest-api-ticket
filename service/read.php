<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow_Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json;");

include_once '../config/database.php';
include_once '../repository/ticket.php';

$database = new Database();
$db = $database->getConnection();

$ticket = new Ticket($db);

$ticket->event_id = isset($_GET['event_id']) ? $_GET['event_id'] : die();
$ticket->ticket_code = isset($_GET['ticket_code']) ? $_GET['ticket_code'] : die();

$ticket->read();

if ($ticket->ticket_code != null) {

    $tickets_arr = array(
        "ticket_code" => $ticket->ticket_code,
        "status" => $ticket->status
    );
    // set response code - 200 OK
    http_response_code(200);
    // response message
    echo json_encode($tickets_arr);
} else {
    // set response code - 404 Not found
    http_response_code(404);
    // response message
    echo json_encode(
        array("message" => "No data found.")
    );
}