<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Method: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../repository/ticket.php';

$database = new Database();
$db = $database->getConnection();

$ticket = new Ticket($db);

// get id of ticket to be edited
$data = json_decode(file_get_contents("php://input"));

$ticket->ticket_code = $data->ticket_code;
$ticket->status = $data->status;
$ticket->event_id = $data->event_id;
$ticket->updated_at = date('Y-m-d H:i:s');

$ticket_arr = array(
    "ticket_code" => $ticket->ticket_code,
    "status" => $ticket->status,
    "updated_at" => $ticket->updated_at
);

if ($ticket->update()) {

    http_response_code(200);

    echo json_encode($ticket_arr);
} else {

    http_response_code(503);

    echo json_encode(array("message" => "Unable to update ticket."));
}